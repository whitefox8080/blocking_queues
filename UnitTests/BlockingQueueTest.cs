﻿using BlockingQueues;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;

namespace UnitTests
{
    
    
    /// <summary>
    ///This is a test class for BlockingQueueTest and is intended
    ///to contain all BlockingQueueTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BlockingQueueTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for BlockingQueue`1 Constructor
        ///</summary>
        public void BlockingQueueConstructorTestHelper<T>()
        {
            int maxSize = 5;
            try
            {
                BlockingQueue<T> target = new BlockingQueue<T>(maxSize);
            }
            catch (ArgumentOutOfRangeException)
            {
                int notExpected = 0;
                Assert.AreNotEqual(notExpected, maxSize, "Zero size is impossible");
            }
            Debug.WriteLine("BlockingQueue is created");
        }

        [TestMethod()]
        public void BlockingQueueConstructorTest()
        {
            BlockingQueueConstructorTestHelper<GenericParameterHelper>();
        }

        /// <summary>
        ///A test for Dequeue
        ///</summary>
        public void DequeueTestHelper<T>()
        {
            int maxSize = 5; // TODO: Initialize to an appropriate value
            BlockingQueue<object> target;
            object value = null;
            object valueExpected = new InputObject();

            int expectedCount = 1;
            int actualCount = 0;

            try
            {
                target = new BlockingQueue<object>(maxSize);
                target.Enqueue(new InputObject());  // First add at least one Packet cause of avoiding the loop
                actualCount = target.Count;
                value = target.Dequeue();
            }
            catch (ArgumentOutOfRangeException)
            {
                int notExpected = 0;
                Assert.AreNotEqual(notExpected, maxSize, "Zero size is impossible");
            }
            Assert.AreEqual(valueExpected.Equals(value), value.Equals(valueExpected));
            Assert.AreEqual(expectedCount, actualCount, "Check is satisfied");
        }

        [TestMethod()]
        public void DequeueTest()
        {
            DequeueTestHelper<GenericParameterHelper>();
        }

        /// <summary>
        ///A test for Enqueue
        ///</summary>
        public void EnqueueTestHelper<T>()
        {
            int maxSize = 5;
            int expectedCount = 1;

            BlockingQueue<object> target = new BlockingQueue<object>(maxSize);
            InputObject obj = new InputObject();
            target.Enqueue(obj);
            Assert.AreEqual(expectedCount, target.Count);
        }

        [TestMethod()]
        public void EnqueueTest()
        {
            EnqueueTestHelper<GenericParameterHelper>();
        }

        /// <summary>
        ///A test for TryDequeue
        ///</summary>
        public void TryDequeueTestHelper<T>()
        {
            int maxSize = 5; // TODO: Initialize to an appropriate value
            BlockingQueue<object> target;
            object value;
            object valueExpected = new InputObject();

            bool expected = true; // We add one element that's why we expect queue is not empty
            bool actual = false;

            int expectedCount = 1;
            int actualCount = 0;

            try
            {
                target = new BlockingQueue<object>(maxSize);

                target.Enqueue(new InputObject());
                actualCount = target.Count;
                actual = target.TryDequeue(out value);
                Assert.AreEqual(valueExpected.Equals(value), value.Equals(valueExpected));
            }
            catch (ArgumentOutOfRangeException)
            {
                int notExpected = 0;
                Assert.AreNotEqual(notExpected, maxSize, "Zero size is impossible");
            }
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expectedCount, actualCount, "Check is satisfied");
        }

        [TestMethod()]
        public void TryDequeueTest()
        {
            TryDequeueTestHelper<GenericParameterHelper>();
        }
    }
}
