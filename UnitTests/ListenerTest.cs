﻿using BlockingQueues;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace UnitTests
{
    
    
    /// <summary>
    ///This is a test class for ListenerTest and is intended
    ///to contain all ListenerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ListenerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for IsStopped
        ///</summary>
        [TestMethod()]
        public void IsStoppedTest()
        {
            IPAddress ipAddress = IPAddress.Parse("0.0.0.0");
            int port = 55;
            BlockingQueue<object> inQueue = new BlockingQueue<object>(5);
            BlockingQueue<object> outQueue = new BlockingQueue<object>(5);
            Listener target = new Listener(ipAddress, port, outQueue, inQueue);
            bool actual;
            bool expected = false;
            actual = target.IsStopped;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Listener Constructor
        ///</summary>
        [TestMethod()]
        public void ListenerConstructorTest()
        {
            Listener listener = null;
            IPAddress ipAddress = IPAddress.Parse("0.0.0.0");
            int port = 55;
            BlockingQueue<object> outQueue = new BlockingQueue<object>(5); 
            BlockingQueue<object> inQueue = new BlockingQueue<object>(5); 
            Listener target = new Listener(ipAddress, port, outQueue, inQueue);
            Assert.AreEqual(outQueue, target.OutQueue);
            Assert.AreEqual(inQueue, target.InQueue);
            Assert.AreNotEqual(listener, target);
        }

        /// <summary>
        ///A test for Start
        ///</summary>
        [TestMethod()]
        public void StartTest()
        {
            IPAddress ipAddress = IPAddress.Parse("0.0.0.0");
            int port = 55;
            BlockingQueue<object> outQueue = new BlockingQueue<object>(5);
            BlockingQueue<object> inQueue = new BlockingQueue<object>(5);

            Listener target = new Listener(ipAddress, port, outQueue, inQueue); // TODO: Initialize to an appropriate value
            Thread notExpected = new Thread(new ThreadStart(target.Run));
            target.Start();
            Assert.AreNotEqual(notExpected.IsAlive, target.ActiveThread.IsAlive);
        }

        /// <summary>
        ///A test for Stop
        ///</summary>
        [TestMethod()]
        public void StopTest()
        {
            bool expected = true;
            IPAddress ipAddress = IPAddress.Parse("0.0.0.0");
            int port = 60;
            BlockingQueue<object> outQueue = new BlockingQueue<object>(5);
            BlockingQueue<object> inQueue = new BlockingQueue<object>(5);
            Listener target = new Listener(ipAddress, port, outQueue, inQueue);
            target.Start();
            target.Stop();
            Assert.AreEqual(expected, target.IsStopped);
        }
    }
}
