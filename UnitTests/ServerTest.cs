﻿using BlockingQueues;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net.Sockets;
using System.Net;
using System.Threading;

namespace UnitTests
{
    
    
    /// <summary>
    ///This is a test class for ServerTest and is intended
    ///to contain all ServerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ServerTest
    {


        private TestContext _testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get => _testContextInstance;
            set => _testContextInstance = value;
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Server Constructor
        ///</summary>
        [TestMethod()]
        public void ServerConstructorTest()
        {
            Server server = null;
            var outQueue = new BlockingQueue<object>(5);
            var inQueue = new BlockingQueue<object>(5);
            var target = new Server(inQueue, outQueue);
            Assert.AreEqual(outQueue, target.OutQueue);
            Assert.AreEqual(inQueue, target.InQueue);
            Assert.AreNotEqual(server, target);
        }

        /// <summary>
        ///A test for Start
        ///</summary>
        [TestMethod()]
        public void StartTest()
        {
            var outQueue = new BlockingQueue<object>(5);
            var inQueue = new BlockingQueue<object>(5);

            var target = new Server(inQueue, outQueue); // TODO: Initialize to an appropriate value
            target.Start();
            var notExpected = new Thread(new ThreadStart(target.Run));
            Assert.AreNotEqual(notExpected.IsAlive, target.ActiveThread.IsAlive);
        }

        /// <summary>
        ///A test for Stop
        ///</summary>
        [TestMethod()]
        public void StopTest()
        {
            var expected = true;
            var outQueue = new BlockingQueue<object>(5);
            var inQueue = new BlockingQueue<object>(5);
            var target = new Server(inQueue, outQueue);
            target.Start();
            target.Stop();
            Assert.AreEqual(expected, target.IsStopped);
        }

        /// <summary>
        ///A test for IsStopped
        ///</summary>
        [TestMethod()]
        public void IsStoppedTest()
        {
            var inQueue = new BlockingQueue<object>(5);
            var outQueue = new BlockingQueue<object>(5);
            var target = new Server(inQueue, outQueue);

            var expected = false;
            var actual = target.IsStopped;
            Assert.AreEqual(expected, actual);
        }
    }
}
