﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading;

namespace BlockingQueues
{
    public class BlockingQueue<T> : IEnumerable<T>
    {
        private Queue<T> _queue = new Queue<T>();

        private readonly int _maxSize;

        public BlockingQueue(int maxSize)
        {
            if (maxSize < 1)
                throw new ArgumentOutOfRangeException("Zero size is impossible");
            _maxSize = maxSize;
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            foreach (T item in _queue)
            {
                yield return item;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<T>)this).GetEnumerator();
        }

        public T Dequeue(int millisecondsTimeout)
        {
            T value;
            lock (_queue)
            {
                while (Count == 0)
                {
                    try
                    {
                        if (!Monitor.Wait(_queue, millisecondsTimeout))
                            throw new QueueTimeoutException();
                    }
                    catch
                    {
                        Monitor.PulseAll(_queue);
                        throw;
                    }
                }
                value = _queue.Dequeue();
                if (Count == (Size - 1))
                    Monitor.PulseAll(_queue);
            }
            return value;
        }

        public T Dequeue()
        {
            return Dequeue(Timeout.Infinite);
        }

        public void Enqueue(T value)
        {
            Enqueue(value, Timeout.Infinite);
        }

        public void Enqueue(T value, int millisecondsTimeout)
        {
            if (value == null)
                throw new ArgumentNullException("Argument is null");
            lock (_queue)
            {
                while (Count == Size)
                {
                    try
                    {
                        if (!Monitor.Wait(_queue, millisecondsTimeout))
                            throw new QueueTimeoutException();
                    }
                    catch
                    {
                        Monitor.PulseAll(_queue);
                        throw;
                    }
                }
                _queue.Enqueue(value);
                if (Count == 1)
                    Monitor.PulseAll(_queue);
            }
        }

        public bool TryDequeue(out T value)
        {
            lock (_queue)
            {
                while (Count == 0)
                {
                    value = default(T);
                    return false;
                }
                value = _queue.Dequeue();
                if (Count == (Size - 1))
                {
                    Monitor.PulseAll(_queue);
                }
                return true;
            }
        }

        public int Size
        {
            get { return _maxSize; }
        }

        public int Count
        {
            get 
            { 
                lock (_queue)
                {
                    return _queue.Count;
                } 
            }
        }
    }

    public class QueueTimeoutException : Exception
    {
        public QueueTimeoutException()
            : base("Time out")
        {
        }
    }
}
