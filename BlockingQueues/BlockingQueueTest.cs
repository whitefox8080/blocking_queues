﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace BlockingQueues
{
    public class Server
    {
        private readonly BlockingQueue<object> _inQueue;			// Inbound packets from listener.
        private readonly BlockingQueue<object> _outQueue;			// Outgoing packets back to listener.
        private Thread _thread;
        private volatile bool _stopped;
        private readonly object _syncRoot = new object();

        public Server(BlockingQueue<object> inQueue, BlockingQueue<object> outQueue)
        {
            if (inQueue == null)
                throw new ArgumentNullException("inQueue is null");
            if (outQueue == null)
                throw new ArgumentNullException("outQueue is null");

            _inQueue = inQueue;
            _outQueue = outQueue;
            _stopped = false;
        }

        public BlockingQueue<object> InQueue
        {
            get { return _inQueue; }
        }

        public BlockingQueue<object> OutQueue
        {
            get { return _outQueue; }
        }

        public Thread ActiveThread
        {
            get { return _thread; }
        }

        public void Start()
        {
            _thread = new Thread(new ThreadStart(Run));
            _thread.Start();
        }

        public void Stop()
        {
            lock (_syncRoot)
            {
                _stopped = true;
            }
        }

        public bool IsStopped
        {
            get
            {
                lock (_syncRoot)
                {
                    return _stopped;
                }
            }
        }

        public void Run()
        {
            Console.WriteLine("Server Started...");
            while (!IsStopped)
            {
                InputObject inObj = _inQueue.Dequeue() as InputObject;
                if (inObj == null)
                    continue;

                Console.WriteLine(String.Format("Server got {0} bytes from client {1}",
                    inObj.Packet.Length,
                    inObj.ipEndPoint.Address.ToString()));

                _outQueue.Enqueue(inObj);
            }
            Console.WriteLine("Server Stopped...");
        }
    }

    public class Listener
    {
        private IPAddress _ipAddress;	// Listen address
        private int _port;				// Listen port
        private Thread _thread;
        private volatile bool _stopped;
        private UdpClient _udpClient;
        private BlockingQueue<object> _outQueue;		// Queue for Packets from the network.
        private BlockingQueue<object> _inQueue;		// Queue that Server thread wants to send back to client.
        private readonly object _syncRoot = new object();

        public Listener(IPAddress ipAddress, int port, BlockingQueue<object> outQueue, BlockingQueue<object> inQueue)
        {
            _thread = new Thread(new ThreadStart(Run));
            _port = port;
            _ipAddress = ipAddress;
            _stopped = false;
            _outQueue = outQueue;
            _inQueue = inQueue;
        }

        public BlockingQueue<object> OutQueue
        {
            get { return _outQueue; }
        }

        public BlockingQueue<object> InQueue
        {
            get { return _inQueue; }
        }

        public Thread ActiveThread
        {
            get { return _thread; }
        }

        public void Start()
        {
            IPEndPoint ipEndPoint = new IPEndPoint(_ipAddress, _port);
            _udpClient = new UdpClient(ipEndPoint);

            _thread.Start();
        }

        public void Stop()
        {
            lock (_syncRoot)
            {
                _stopped = true;
                _udpClient.Close();
            }
        }

        public bool IsStopped
        {
            get
            {
                lock (_syncRoot)
                {
                    return _stopped;
                }
            }
        }

        public void Run()
        {
            Console.WriteLine("Listener Started...");
            IPEndPoint remoteEndPoint = null;
            byte[] buffer = null;

            while (!IsStopped)
            {
                Thread.Sleep(800);

                remoteEndPoint = new IPEndPoint(IPAddress.Parse("192.168.0.2"), 1983);
                buffer = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
                if (buffer == null || buffer.Length == 0)
                    continue;

                InputObject iObj = new InputObject();
                iObj.Packet = buffer;
                iObj.ipEndPoint = remoteEndPoint;

                _outQueue.Enqueue(iObj);

                object outObj;
                if (!_inQueue.TryDequeue(out outObj))
                    continue;
                InputObject tmpObj = outObj as InputObject;
                if (tmpObj == null)
                    continue;
                Console.WriteLine("Sending data to client " + tmpObj.ipEndPoint.Address.ToString());
                outObj = null;
            }
            Console.WriteLine("Listener Stopped...");
        }
    }

    public class InputObject
    {
        public byte[] Packet;
        public IPEndPoint ipEndPoint;
    }
}