﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading;

namespace BlockingQueues
{
    class Program
    {
        static void Main(string[] args)
        {
            var inQueue = new BlockingQueue<object>(10);	// Packets to send back to client.
            var outQueue = new BlockingQueue<object>(10);	// Packets from Listener.
            var listener = new Listener(IPAddress.Parse("0.0.0.0"), 55, outQueue, inQueue);
            var server = new Server(outQueue, inQueue);
            server.Start();
            listener.Start();
            Thread.Sleep(10000); // Time for sending/receiving packets
            listener.Stop();
            server.Stop();
        }
    }
}
